package dev.zordon.common;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class RotatingQueue<T>
  {
    T [] elements;
    
    @SuppressWarnings("unchecked")
    public RotatingQueue(Class<T> clazz,int size)
    {
      elements = (T[]) Array.newInstance(clazz, size);
    }
    
    public void add(T element)
    {
      for(int i=elements.length-1;i>0;i--)
        elements[i] = elements[i-1];

      elements[0] = element;
    }
    
     public T[] getAll()
     {
       return elements;
     }
     
     public T get(int index)
     {
    	 return elements[index];
     }
     
     public String dump()
     {
    	 return Arrays.toString(elements);
     }
     
    
  }

