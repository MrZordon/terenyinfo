package dev.zordon.gc2.tereny;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import dev.zordon.common.RotatingQueue;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.model.pipeline.BlockInfo;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

public class TerenyInfo 
{
	
	private final long msToExpire = 1000L * ((25L * 24L * 60L * 60L) - (3L * 60L * 60L) - (24L * 60L));
	private Minecraft mc;
	
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
	StringBuilder sb = new StringBuilder( 120 );
	
	
			
	public TerenyInfo( Minecraft mc)
	{
		this.mc = mc;
		
		registerHandlers();
		registerHotkeys();
		
		rm = mc.getRenderManager();
	}
	
	
	KeyBinding keybindShowInfo;
	
	private void registerHotkeys() 
	{
		keybindShowInfo = new KeyBinding("TerenInfo", Keyboard.KEY_SEMICOLON, "key.categories.misc");

		ClientRegistry.registerKeyBinding(keybindShowInfo);
	}

	private void registerHandlers()
	{
		MinecraftForge.EVENT_BUS.register( this );
		FMLCommonHandler.instance().bus().register( this );
	}
	
	long chunkDisplayTime = 5000;
	long minimumCommandDelay = 2000;
	
	long processCommandAfter = 0;
	long drawBordersBefore = 0;
	
	@SubscribeEvent
	public void onHotkey( KeyInputEvent event )
	{
		if ( keybindShowInfo.isPressed() && Minecraft.getSystemTime() > processCommandAfter )
		{
			mc.thePlayer.sendChatMessage("/teren info");
			
			chunkX = mc.thePlayer.chunkCoordX;
			chunkZ = mc.thePlayer.chunkCoordZ;
			
			drawBordersBefore = Minecraft.getSystemTime() + chunkDisplayTime;
			
			processCommandAfter = Minecraft.getSystemTime() + minimumCommandDelay;
		}
	}

	long borderTimer = 0;
	
	// 
	int chunkX = 0;
	int chunkZ = 0;
	private boolean shouldDrawBorders = false;
	
	
	
	public static void drawBoundingBoxFloor(AxisAlignedBB aa, double staticY) 
	{
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldRenderer = tessellator.getWorldRenderer();

		worldRenderer.startDrawing(3);
		worldRenderer.addVertex(aa.minX, staticY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, staticY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, staticY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, staticY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, staticY, aa.minZ);
		worldRenderer.addVertex(aa.minX, staticY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, staticY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, staticY, aa.minZ);
		
		tessellator.draw();
	}
	
	RenderManager rm;
	
	@SubscribeEvent
	public void drawChunkBorders( RenderWorldLastEvent event )
	{
		if ( Minecraft.getSystemTime() < drawBordersBefore )
		{
			double posX = chunkX * 16;
			double posZ = chunkZ * 16;
			
			double posY = (int) mc.thePlayer.posY;
			
			GL11.glLineWidth(1f);
			GL11.glColor4f( 1f,1f,1f,1f);
			GL11.glDepthMask(false);
			
			GL11.glDisable(3553);
		    GL11.glDisable(2929);
			
			drawBoundingBoxFloor( 
					new AxisAlignedBB(
							posX - rm.viewerPosX, 
							0,
							posZ - rm.viewerPosZ,
							posX + 16  - rm.viewerPosX,
							0,
							posZ + 16  - rm.viewerPosZ
							
							), posY - rm.viewerPosY);
			
			GL11.glDepthMask(true);
			
			GL11.glEnable(3553);
		    GL11.glEnable(2929);
		}
	}
	
	RotatingQueue<String> chatHistory = new RotatingQueue<String>(String.class,5);
	
	@SubscribeEvent
	public void onChatLine(ClientChatReceivedEvent event)
	{
		String chatLine = event.message.getUnformattedText();
		
		chatHistory.add(chatLine);
		
		if ( chatLine.startsWith(" - OSTATNIA AKTYWNOŚĆ:") )
		{
			//String [] history = chatHistory.getAll();
			
			String owner   = chatHistory.get(2).substring(15);
			String friends = chatHistory.get(1).substring(18);
			
			//System.out.println("Owner   : " + owner);
			//System.out.println("Friends : " + friends);
			
			
			chunkX = mc.thePlayer.chunkCoordX;
			chunkZ = mc.thePlayer.chunkCoordZ;
			
			drawBordersBefore = Minecraft.getSystemTime() + chunkDisplayTime;
			
			// nie wyswietlamy linijki z czatu, pokazemy wlasna
			event.setCanceled(true);
			
			int datePos = 23;
						
			String dayAndTimeAsString = chatLine.substring( datePos);
			
		     try 
		     {
				long lastUpdateDate = formatter.parse(dayAndTimeAsString).getTime();
				
				long expiryDate = lastUpdateDate;
				
				expiryDate += msToExpire;
				logChunkExpiration( chunkX, chunkZ, expiryDate, owner );				
				long diff = (expiryDate - new Date().getTime());

				long seconds = diff / 1000L;
				long minutes = seconds / 60L;

				seconds %= 60L;

				long hours = minutes / 60L;

				minutes %= 60L;

				long days = hours / 24L;

				hours %= 24L;
				
				sb.setLength(0);
				sb.append("§b§l - §l§lWYGASA ZA §b§a");
				sb.append(days);
				sb.append("d ");
				sb.append(hours);
				sb.append("h ");
				sb.append(minutes);
				sb.append("m ");
				sb.append(seconds);
				sb.append("s ");

				
				mc.thePlayer.addChatMessage( new ChatComponentText( "§b§l" + chatLine) );
				mc.thePlayer.addChatMessage( new ChatComponentText( sb.toString() ));
				
				
				
				
			} catch (ParseException e) {}
	
		}
	}

	private void logChunkExpiration(int chunkX, int chunkZ, long expiryDate, String owner) 
	{
		System.out.printf("[TERENY INFO] Chunk owner %s (%d,%d) expires at %d\n", owner, chunkX, chunkZ, expiryDate);
		
	}
	
}
