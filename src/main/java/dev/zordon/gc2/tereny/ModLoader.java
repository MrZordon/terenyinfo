package dev.zordon.gc2.tereny;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod( modid= ModLoader.MODID, version = ModLoader.VERSION )
public class ModLoader 
{
	public static final String MODID =   "TerenyInfo";
	public static final String VERSION = "1.0";
	
	private TerenyInfo mod;

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		mod = new TerenyInfo( Minecraft.getMinecraft() );
		
		
	}
	
}
